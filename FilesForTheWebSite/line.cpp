#ifndef LINESTRING_H
#define LINESTRING_H

#include <iostream>
#include <vector>


class Drawer;

class LineString : public Geometry {
	private:
		std::vector<float> vVertices;

	public:
		/**
		 * @brief constructor of linestring
		 */
		LineString(std::vector<float> vVertices);

		/**
		 * @brief destructor of linestring
		 */
		~LineString();

		/**
		 * @brief calculate the length of a linestring
		 *
		 * @return double length of the linestring
		 */
		double length();

		/**
		 * @brief get the vector of vertices building the linestring
		 *
		 * @return std::vector<float> vector of vertices building the linestring
		 */
		std::vector<float> getvVertices();

		/**
		 * @brief do an operation of a visitor
		 *
		 * @param visitor Visitor
		 */
		void draw(Drawer *drawer);

	};

#endif



#include <cmath>
#include <algorithm>

#include "LineString.hpp"
#include "Drawer.hpp"

LineString::LineString(std::vector<float> vVertices) : vVertices(vVertices){

}

LineString::~LineString() {

}

std::vector<Float> LineString::getvVertices() {
    return vVertices;
}

double LineString::length(){
    double length = 0;
    for(int i = 0; i<vVertices.size() - 1; i++){
        if ( i%3 == 0 && (i+5) < vVertices.size() ){
			 length += sqrt(pow((vVertices[i] - vVertices[i+3] ),2) 
			+ pow((vVertices[i+1] - vVertices[i+4] ),2) 
			+ pow((vVertices[i+2] - vVertices[i+5] ),2));  
		}
             
    }
    return length;
}

void LineString::draw(Drawer* drawer) {
    drawer->drawLinestring(this);    
}

std::vector<double> LineString::getBoundingBox(){
    std::vector<double> bbox;
    double xmin = 0, xmax = 0, ymin = 0, ymax = 0;
    for(auto &point: vPoints){
        xmin = std::min(xmin, point.getBoundingBox()[0]);
        xmax = std::max(xmax, point.getBoundingBox()[1]);
        ymin = std::min(ymin, point.getBoundingBox()[2]);
        ymax = std::max(ymax, point.getBoundingBox()[3]);
    }
    bbox.push_back(xmin);
    bbox.push_back(xmax);
    bbox.push_back(ymin);
    bbox.push_back(ymax);
    return bbox;
};
