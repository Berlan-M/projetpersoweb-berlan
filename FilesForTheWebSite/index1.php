<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style2.css">
    <title>Document</title>
</head>
<body>
<?php

include "connect.php";

$requete_category = pg_query($link, 'SELECT id, name FROM category');

$requete_artists = pg_query($link, 'SELECT id,name FROM artist');


//Select your representation by category or by artist name


echo '

 <div id="cadre_general">

    <div id="cadre_artiste"> 
        <div id="name_artiste">
            <p > <strong>Chosissez l artiste</strong> </p>
        </div>

        <div>
        <form method="post" action="index2.php">
        <select name="artist" id="a_names">   
        ';
          if ($requete_artists) {
            while ($ligne = pg_fetch_row($requete_artists)) {

              echo '<option value=\''.$ligne[1].'\' name = "artist">'.$ligne[1].'</option>';
              
            }
          } else {
            echo "Erreur de requête de base de données.";
          };
echo  '
        </select>
        <div>
        <input type="submit" value="Ok">
        </div>
        </form>
        </div>
    </div>

    <div id="cadre_categorie"> 
        <div id="name_categorie">
            <p> <strong>Chosissez la categorie</strong> </p>
        </div>

        <div>
        <form  method="post" action="index2.php">
        <select name="categories" id="c_names">
         ';
        if ($requete_category) {
          while ($ligne = pg_fetch_row($requete_category)) {

            echo '<option value=\''.$ligne[0].'\' name = "categories">'.$ligne[1].'</option>';
            
          }
        } else {
          echo "Erreur de requête de base de données.";
        };
echo '
        </select>
        <div>
        <input type="submit" value="Ok">
        </div>
        </form>
        </div>
    </div>

</div>
'
?>
</body>
</html>