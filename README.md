# ProjetPersoWeb-Berlan

Projet personnel Web

#Requirements

PostgresQl11, a local server with php, a text editor, a web navigator

#How to Install and use

1. Install PostgresQl
2. Create a  new dataBase
3. Clone the gitlab repository
4. Restore your dataBase with db.backup or dataBase.sql. You will find those files in "BasedeDonnees" directory
5. Start your local server
6. Copy all files of "FilesForTheWebSite" directory and paste them into your local server repository
7. Open the file "connect.php", write your connection's informations 
8. Now you can write "localhost/index.html" in any navigator and enjoy the web site

#Files & directories

1. Files :  All files (Html, php, JavaScript, jpg images and repositories for librairies used)
2. BasedeDonnees :  Files for the database (backup and sql)

#Author
Berlan MALANDA

