--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 13.0

-- Started on 2020-12-06 06:28:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 209 (class 1259 OID 19034)
-- Name: admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin (
    id character varying,
    password character varying
);


ALTER TABLE public.admin OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 18984)
-- Name: artist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.artist (
    id integer NOT NULL,
    name character varying,
    id_category integer,
    presentations character varying
);


ALTER TABLE public.artist OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 18982)
-- Name: artist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.artist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.artist_id_seq OWNER TO postgres;

--
-- TOC entry 2898 (class 0 OID 0)
-- Dependencies: 204
-- Name: artist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.artist_id_seq OWNED BY public.artist.id;


--
-- TOC entry 203 (class 1259 OID 18973)
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.category OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 18971)
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- TOC entry 2899 (class 0 OID 0)
-- Dependencies: 202
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- TOC entry 207 (class 1259 OID 19003)
-- Name: costumer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.costumer (
    name character varying,
    pernom character varying,
    email character varying,
    ticket character varying,
    ticketisvalid boolean,
    dateticket character varying,
    horaireticket character varying,
    id integer NOT NULL,
    spectacle character varying,
    place integer
);


ALTER TABLE public.costumer OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 19023)
-- Name: costumer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.costumer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.costumer_id_seq OWNER TO postgres;

--
-- TOC entry 2900 (class 0 OID 0)
-- Dependencies: 208
-- Name: costumer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.costumer_id_seq OWNED BY public.costumer.id;


--
-- TOC entry 196 (class 1259 OID 18938)
-- Name: presentation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.presentation (
    id_category integer,
    name character varying,
    artist character varying,
    schedules character varying,
    number_of_seats integer,
    img character varying,
    date character varying,
    id integer NOT NULL
);


ALTER TABLE public.presentation OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 18949)
-- Name: presentation_hall; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.presentation_hall (
    id integer NOT NULL,
    name character varying,
    adress character varying,
    number_of_seat integer
);


ALTER TABLE public.presentation_hall OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 18947)
-- Name: presentation_hall_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.presentation_hall_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.presentation_hall_id_seq OWNER TO postgres;

--
-- TOC entry 2901 (class 0 OID 0)
-- Dependencies: 197
-- Name: presentation_hall_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.presentation_hall_id_seq OWNED BY public.presentation_hall.id;


--
-- TOC entry 210 (class 1259 OID 19040)
-- Name: presentation_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.presentation_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.presentation_id_seq1 OWNER TO postgres;

--
-- TOC entry 2902 (class 0 OID 0)
-- Dependencies: 210
-- Name: presentation_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.presentation_id_seq1 OWNED BY public.presentation.id;


--
-- TOC entry 201 (class 1259 OID 18965)
-- Name: reservation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservation (
    id integer NOT NULL,
    ticket integer,
    costumer_name character(1)
);


ALTER TABLE public.reservation OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 18963)
-- Name: reservation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservation_id_seq OWNER TO postgres;

--
-- TOC entry 2903 (class 0 OID 0)
-- Dependencies: 200
-- Name: reservation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reservation_id_seq OWNED BY public.reservation.id;


--
-- TOC entry 199 (class 1259 OID 18958)
-- Name: schedules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schedules (
    id integer NOT NULL,
    "time" time without time zone
);


ALTER TABLE public.schedules OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 18993)
-- Name: seat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seat (
    id integer NOT NULL,
    number integer,
    "position" character varying
);


ALTER TABLE public.seat OWNER TO postgres;

--
-- TOC entry 2738 (class 2604 OID 18987)
-- Name: artist id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artist ALTER COLUMN id SET DEFAULT nextval('public.artist_id_seq'::regclass);


--
-- TOC entry 2737 (class 2604 OID 18976)
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- TOC entry 2739 (class 2604 OID 19025)
-- Name: costumer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.costumer ALTER COLUMN id SET DEFAULT nextval('public.costumer_id_seq'::regclass);


--
-- TOC entry 2734 (class 2604 OID 19042)
-- Name: presentation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentation ALTER COLUMN id SET DEFAULT nextval('public.presentation_id_seq1'::regclass);


--
-- TOC entry 2735 (class 2604 OID 18952)
-- Name: presentation_hall id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentation_hall ALTER COLUMN id SET DEFAULT nextval('public.presentation_hall_id_seq'::regclass);


--
-- TOC entry 2736 (class 2604 OID 18968)
-- Name: reservation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation ALTER COLUMN id SET DEFAULT nextval('public.reservation_id_seq'::regclass);


--
-- TOC entry 2891 (class 0 OID 19034)
-- Dependencies: 209
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.admin (id, password) FROM stdin;
tsi	2020
\.


--
-- TOC entry 2887 (class 0 OID 18984)
-- Dependencies: 205
-- Data for Name: artist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.artist (id, name, id_category, presentations) FROM stdin;
1	Dieudonne	3	La guerre -La politique-Les média -Foxtrot -Mahmoud -Le divorce de Patrick -Rendez-nous Jésus
2	Jamel Debouze	3	Maintenant ou Jamel -Tout sur Jamel -Jamel en scène
4	Libera	1	Sanctus -Going home -Far away -Loces iste
5	Booba	1	Temps mort- Pantheon -Ouest side -Lunatic -Futur -Trone 
6	Jonny Hallyday	1	Salut les copains -Jeune Homme -Vie -Flagrant delit -Insolitudes
7	Ismael le magicien	5	Darkness -Pigeons -Corbeaux
9	David Copperfiel	5	Grande illusion -The Incredible Burt Wonderstone
10	Viktor Vincent	4	Les liens invisibles -Emprise -Synapes -Arcane
11	Jean Baptiste	4	Miracle -Illusion
12	Orlinski	2	Le kabaret -Danse with us
13	Eleonardo Abbagnato	2	Les 4 saisons -Just Dance
\.


--
-- TOC entry 2885 (class 0 OID 18973)
-- Dependencies: 203
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, name) FROM stdin;
1	Spectacle de musiqe
2	Spectacle de danse
3	Spectacle comique
4	Spectacle de mentalisme
5	Spectacle de prestidigitation
\.


--
-- TOC entry 2889 (class 0 OID 19003)
-- Dependencies: 207
-- Data for Name: costumer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.costumer (name, pernom, email, ticket, ticketisvalid, dateticket, horaireticket, id, spectacle, place) FROM stdin;
vic	coi	 \r\n        hegf	v5d5hUwdm5	t	14-03-2021	17h 	9652	Foxtrot	134
malanda	Berlan	 \r\n        ber@	xO1uuwtZD2	f	15-04-2020	01h	9651	The Incredible Burt Wonderstone	28
efe	fefe	 \r\n        fefe	1O6VtuPmIy	t	17-12-2020	15h 	9653	Le divorce de Patrick	3
efe	fefe	 \r\n        fefe	yYJnrUtxIC	t	17-12-2020	15h 	9654	Le divorce de Patrick	102
\.


--
-- TOC entry 2878 (class 0 OID 18938)
-- Dependencies: 196
-- Data for Name: presentation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.presentation (id_category, name, artist, schedules, number_of_seats, img, date, id) FROM stdin;
3	Maintenant ou Jamel	Jamel Debouze	10h -12h -14h	200	maintenantoujamel.jpg	18-12-2020_18-02-2021_18-03-2021	3
3	Tout sur Jamel	Jamel Debouze	10h -12h -14h	200	toutsurjamel.jpg	19-12-2020_19-02-2021_19-03-2021	4
1	Temps mort	Booba	22h	200	tempsmort.jpg	21-12-2020_21-02-2021_21-03-2021	5
1	Pantheon	Booba	22h	200	pantheon.jpg	22-12-2020_22-02-2021_22-03-2021	6
1	Lunatic	Booba	22h	200	lunatic.jpg	24-12-2020_24-02-2021_24-03-2021	7
1	Trone	Booba	22h	200	trone.jpg	26-12-2020_26-02-2021_26-03-2021	8
1	Sanctus	Libera	08h	200	sanctus.jpg	27-12-2020_27-02-2021_27-03-2021	9
1	Going home	Libera	08h	200	goinghome.jpg	28-12-2020_28-02-2021_28-03-2021	10
1	Far away	Libera	08h	200	faraway.jpg	05-12-2020_05-02-2021_05-03-2021	11
1	Loces iste	Libera	08h	200	loces iste.jpg	06-12-2020_06-02-2021_06-03-2021	12
1	Salut les copains	Jonny Hallyday	09h	200	salutlescopains.jpg	07-12-2020_07-02-2021_07-03-2021	13
1	Jeune Homme	Jonny Hallyday	09h	200	jeunehomme.jpg	08-12-2020_08-02-2021_08-03-2021	14
1	Vie	Jonny Hallyday	09h	200	vie.jpg	09-12-2020_09-02-2021_09-03-2021	15
1	Flagrant delit	Jonny Hallyday	09h	200	flagrantdelit.jpg	10-12-2020_10-02-2021_10-03-2021	16
1	Darkness	Ismael le magicien	11h	200	darkness.jpg	11-12-2020_11-02-2021_11-03-2021	17
5	Pigeons	Ismael le magicien	11h	200	pigeons.jpg	12-04-2020_12-02-2021_12-03-2021	18
5	Corbeaux	Ismael le magicien	11h	200	corbeaux.jpg	13-04-2020_13-02-2021_13-03-2021	19
5	Grande illusion	David Copperfiel	01h	200	grandillusion.jpg	14-04-2020_14-05-2021_14-06-2021	20
4	Les liens invisibles	Viktor Vincent	02h	200	lesliensinvisibles.jpg	16-04-2020_16-05-2021_16-06-2021	22
4	Emprise	Viktor Vincent	02h	200	emprise.jpg	17-04-2020_17-05-2021_17-06-2021	23
4	Synapes	Viktor Vincent	02h	200	synapes.jpg	18-04-2020_18-05-2021_18-06-2021	24
4	Arcane	Viktor Vincent	02h	200	arcane.jpg	19-04-2020_19-05-2021_19-06-2021	25
5	The Incredible Burt Wonderstone	David Copperfiel	01h	200	TheIncredibleBurtWonderstone.jpg	15-04-2020_15-05-2021_15-06-2021	21
3	Foxtrot	Dieudonne	15h -17h -19h	199	foxtrot.jpg	14-12-2020_14-02-2021_14-03-2021	2
3	Le divorce de Patrick	Dieudonne	15h -17h -19h	198	ledivorcedepatrick.jpg	17-12-2020_17-02-2021_17-03-2021	1
4	Miracle	Jean Baptiste	03h	200	miracle.jpg	20-04-2020_20-05-2021_20-06-2021	26
4	Illusion	Jean Baptiste	03h	200	illusion.jpg	21-04-2020_21-05-2021_21-06-2021	27
2	Danse with us	Orlinski	04h	200	dansewithus.jpg	23-04-2020_23-05-2021_23-06-2021	29
2	Les 4 saisons 	Eleonardo Abbagnato	05h	200	les4saisons.jpg	24-04-2020_24-05-2021_24-06-2021	30
2	Just Dance	Eleonardo Abbagnato	05h	200	justdance.jpg	25-04-2020_25-05-2021_25-06-2021	31
1	Ouest side	Booba	22h	200	ouestside.jpg	23-12-2020_23-02-2021_23-03-2021	32
3	La politique	Dieudonne	15h -17h -19h	200	lapolitique.jpg	13-12-2020_13-02-2021_13-03-2021	33
3	La guerre	Dieudonne	15h -17h -19h	200	laguerre.jpg	12-12-2020_12-02-2021_12-03-2021	34
1	dghegde	\N	dkejhde	200	dkehd	dejgde	35
2	Le kabaret	Orlinski	04h	200	lekabaret.jpg	22-04-2020_22-05-2021_22-06-2021	28
\.


--
-- TOC entry 2880 (class 0 OID 18949)
-- Dependencies: 198
-- Data for Name: presentation_hall; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.presentation_hall (id, name, adress, number_of_seat) FROM stdin;
1	ENSG-SHOW	7-Avenue-Blaise-Pascal-77420	250
\.


--
-- TOC entry 2883 (class 0 OID 18965)
-- Dependencies: 201
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservation (id, ticket, costumer_name) FROM stdin;
\.


--
-- TOC entry 2881 (class 0 OID 18958)
-- Dependencies: 199
-- Data for Name: schedules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.schedules (id, "time") FROM stdin;
\.


--
-- TOC entry 2888 (class 0 OID 18993)
-- Dependencies: 206
-- Data for Name: seat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.seat (id, number, "position") FROM stdin;
\.


--
-- TOC entry 2904 (class 0 OID 0)
-- Dependencies: 204
-- Name: artist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.artist_id_seq', 1, false);


--
-- TOC entry 2905 (class 0 OID 0)
-- Dependencies: 202
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 1, false);


--
-- TOC entry 2906 (class 0 OID 0)
-- Dependencies: 208
-- Name: costumer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.costumer_id_seq', 9654, true);


--
-- TOC entry 2907 (class 0 OID 0)
-- Dependencies: 197
-- Name: presentation_hall_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.presentation_hall_id_seq', 1, false);


--
-- TOC entry 2908 (class 0 OID 0)
-- Dependencies: 210
-- Name: presentation_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.presentation_id_seq1', 35, true);


--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 200
-- Name: reservation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservation_id_seq', 1, false);


--
-- TOC entry 2751 (class 2606 OID 18992)
-- Name: artist artist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artist
    ADD CONSTRAINT artist_pkey PRIMARY KEY (id);


--
-- TOC entry 2749 (class 2606 OID 18981)
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 2741 (class 2606 OID 19050)
-- Name: presentation id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentation
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- TOC entry 2755 (class 2606 OID 19033)
-- Name: costumer id_costumer; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.costumer
    ADD CONSTRAINT id_costumer PRIMARY KEY (id);


--
-- TOC entry 2743 (class 2606 OID 18957)
-- Name: presentation_hall presentation_hall_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentation_hall
    ADD CONSTRAINT presentation_hall_pkey PRIMARY KEY (id);


--
-- TOC entry 2747 (class 2606 OID 18970)
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id);


--
-- TOC entry 2745 (class 2606 OID 18962)
-- Name: schedules schedules_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schedules
    ADD CONSTRAINT schedules_pkey PRIMARY KEY (id);


--
-- TOC entry 2753 (class 2606 OID 19000)
-- Name: seat seat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seat
    ADD CONSTRAINT seat_pkey PRIMARY KEY (id);


--
-- TOC entry 2756 (class 2606 OID 19017)
-- Name: artist artist_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.artist
    ADD CONSTRAINT artist_fk FOREIGN KEY (id_category) REFERENCES public.category(id);


-- Completed on 2020-12-06 06:28:18

--
-- PostgreSQL database dump complete
--

